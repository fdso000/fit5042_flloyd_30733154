/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author Junyang
 * @author Flloyd Dsouza
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{

	 private List<Property> propertyList;   
	
    public SimplePropertyRepositoryImpl() {
    	propertyList = new ArrayList<Property>();
    }

    
    
	public SimplePropertyRepositoryImpl(List<Property> propertyList) {
		super();
		this.propertyList = propertyList;
	}


	

	public List<Property> getPropertyList() {
		return propertyList;
	}



	public void setPropertyList(List<Property> propertyList) {
		this.propertyList = propertyList;
	}



	@Override
	public void addProperty(Property property) throws Exception {
		propertyList.add(property);
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		Property property = null;
		for (Property p : propertyList) {
			if (p.getId() == id)
				 property = p;
		}
		return property;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
			return propertyList;
	}
    
}
