package fit5042.tutex;
/**
 * @author Flloyd Dsouza
 */

import fit5042.tutex.repository.PropertyRepository;
import fit5042.tutex.repository.PropertyRepositoryFactory;
import fit5042.tutex.repository.entities.Property;
import java.util.List;
import java.util.Scanner;

public class RealEstateAgency {
    private String name;
    private final PropertyRepository propertyRepository;

    public RealEstateAgency(String name) throws Exception {
        this.name = name;
        this.propertyRepository = PropertyRepositoryFactory.getInstance();
    }
    
    // this method is for initializing the property objects
    public void createProperty() throws Exception {
        Property p1 = new Property(1,"26 Batesford Road, Malvern East, VIC 3145",3,150,420000.00);
        Property p2 = new Property(2,"5/16 Wattle Avenu, Glen Huntly, VIC 3163",4,250,650000.00);
        Property p3 = new Property(3,"2/8 Derby Crecent, Caulfield East, VIC 3145",2,150,750000.00);
        Property p4 = new Property(4,"1/6 Coorigal Road, Carniegie, VIC 3163",4,250,850000.00);
        Property p5 = new Property(5,"26 Lorne St, Caulfield East, VIC 3145",4,350,250000.00);
        
        propertyRepository.addProperty(p1);
        propertyRepository.addProperty(p2);
        propertyRepository.addProperty(p3);
        propertyRepository.addProperty(p4);
        propertyRepository.addProperty(p5);
        System.out.println("5 Properties added successfully.");
    }
    
    // this method is for displaying all the properties
    public void displayProperties() throws Exception {
    	List<Property> propertyList  = propertyRepository.getAllProperties();
    	for (Property p : propertyList) {
    		System.out.println(p.toString());
    	}
    }
    
    // this method is for searching the property by ID
    public void searchPropertyById() throws Exception {
    	System.out.println("Enter the ID of the property you want to search:");
    	Scanner sc = new Scanner(System.in);
    	int i = sc.nextInt();
    	Property p = propertyRepository.searchPropertyById(i);
    	if(p!= null)
    		System.out.println(p.toString());
    	else
    		System.out.println("Property Not Found");
    	
    }
    
    public void run() throws Exception {
        createProperty();
        System.out.println("********************************************************************************");
        displayProperties();
        System.out.println("********************************************************************************");
        searchPropertyById();
    }
    
    public static void main(String[] args) {
        try {
            new RealEstateAgency("FIT5042 Real Estate Agency").run();
        } catch (Exception ex) {
            System.out.println("Application fail to run: " + ex.getMessage());
        }
    }
}
