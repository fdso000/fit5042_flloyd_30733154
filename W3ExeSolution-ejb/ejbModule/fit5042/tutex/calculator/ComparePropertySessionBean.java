package fit5042.tutex.calculator;

import java.rmi.RemoteException;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.CreateException;
import javax.ejb.Stateful;

import fit5042.tutex.repository.entities.Property;
@Stateful
public class ComparePropertySessionBean implements CompareProperty  {
	private Set<Property> list;
	
	/*
	 * This method adds the property to the comparison List
	 */
	@Override
	public void addProperty(Property property) {
		list.add(property);
	}

	
	/*
	 * This method returns the property having highest number of rooms from the comparison List
	 */	
	@Override
	public int bestPerRoom() {
		Integer bestID=0;
        int numberOfRooms;
        double price;
        double bestPerRoom=100000000.00;
        for(Property p : list)
        {
            numberOfRooms = p.getNumberOfBedrooms();
            price = p.getPrice();
            if(price / numberOfRooms < bestPerRoom)
            {
                bestPerRoom = price / numberOfRooms;
                bestID = p.getPropertyId();
            }
        }
        return bestID;
	}

	
	/*
	 * This method removes the property from the comparison List
	 */	
	@Override
	public void removeProperty(Property property) {
		for(Property p : list) {
			if(p.getPropertyId() == property.getPropertyId()) {
				list.remove(p);
				break;
			}
		}
	}

		@PostConstruct
	    public void init() {
	        list=new HashSet<>();
	    }

	    public CompareProperty create() throws CreateException, RemoteException {
	        return null;
	    }

	    public void ejbCreate() throws CreateException {
	    }

}
