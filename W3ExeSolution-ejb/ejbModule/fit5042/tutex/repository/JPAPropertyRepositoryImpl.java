package fit5042.tutex.repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.Stateless;

import fit5042.tutex.repository.constants.CommonInstance;
import fit5042.tutex.repository.entities.ContactPerson;
import fit5042.tutex.repository.entities.Property;

@Stateless
public class JPAPropertyRepositoryImpl implements PropertyRepository {
	private ArrayList<Property> propertyList;
	
	public JPAPropertyRepositoryImpl() {
    	propertyList = new ArrayList<Property>();
    	this.initialisePropertyList();
    }
	
	public void initialisePropertyList() {
    	propertyList.clear();    	
    	propertyList.add(CommonInstance.PROPERTY_FIRST);
    	propertyList.add(CommonInstance.PROPERTY_SECOND);
    	propertyList.add(CommonInstance.PROPERTY_THIRD);
    	propertyList.add(CommonInstance.PROPERTY_FOURTH);
    }

	public ArrayList<Property> getPropertyList() {
		return propertyList;
	}

	public void setPropertyList(ArrayList<Property> propertyList) {
		this.propertyList = propertyList;
	}
	
	public void removeProperty(int propertyId) {
    	for (Property p : propertyList) {
    		if (p.getPropertyId() == propertyId) {
    			propertyList.remove(p);
    			break;
    		}
    	}
    	
    }
    
	
	
    public void addProperty(Property property) {
    	propertyList.add(property);
    }
    
    public void editProperty(Property property) {
    	for (Property p : propertyList) {
    		int id = property.getPropertyId();
    		if (p.getPropertyId() == id) {
    			propertyList.set(id, property);
    			break;
    		}
    	}
    }

	public int getPropertyId() {
		return propertyList.get(propertyList.size() - 1).getPropertyId();
	}
	
	public Property searchPropertyById(int propertyId) {
		for (Property p : propertyList) {
    		if (p.getPropertyId() == propertyId) {
    			return p;
    		}
    	}
		return null;
	}
	
	
	/*
	 * This method returns a list of all contact Persons of all properties.
	 * This method first uses a for each loop to get persons from each Property and add it to a HashSet
	 * Due to this there are no duplicate values.
	 * Then again we run a for each loop on the hash set to transfer the items to the List  
	 */
		public List<ContactPerson> getAllContactPeople() {
		Set<ContactPerson> contactPersonSet = new HashSet<>();		
		for (Property p : propertyList) {
			contactPersonSet.add(p.getContactPerson());
		}
		List<ContactPerson> contactPersonList = new ArrayList<>();		
		for (ContactPerson c : contactPersonSet) {
			contactPersonList.add(c);
		}		
		return contactPersonList;
	}
	
		
		
		
	/*
	 * This method takes in a contact person and returns a set of properties having same contact person
	 * 	
	 */
	public Set<Property> searchPropertyByContactPerson(ContactPerson contactPerson) {
		Set<Property> propertySet = new HashSet<>();
		
		for (Property p : propertyList) {
			if (p.getContactPerson().equals(contactPerson)) {
				propertySet.add(p);
			}
    	}
		
		return propertySet;
	}
	
	
	/*
	 * This method takes  a parameter of type double.
	 * It checks if the property price is less that or equal to the provided budget
	 * If the price of the property is less than or equal it add it to the property list and returns the list
	 */
	public List<Property> searchPropertyByBudget(double budget) {
		List<Property> properties = new ArrayList<>();		
		for (Property property : propertyList) {
			if (property.getPrice() <= budget) {
				properties.add(property);
			}
		}		
		return properties;
    }

}
