package fit5042.tutex.calculator;

import java.rmi.RemoteException;

import javax.ejb.Remote;
import javax.ejb.CreateException;

import fit5042.tutex.repository.entities.Property;

/*
 * Added Remote Annotation
 * And Created interface for Compare property with Methods
 */

@Remote
public interface CompareProperty {

	void addProperty(Property property);
	int bestPerRoom();
	void removeProperty(Property property);
	CompareProperty create() throws CreateException, RemoteException;  
	
}
