/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.mbeans;

import java.io.Serializable;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.enterprise.context.SessionScoped;

import fit5042.tutex.repository.entities.Loan;
import fit5042.tutex.calculator.MonthlyPaymentCalculator;

/**
 *
 * @author: originally created by Eddie. The following code has been changed in
 * order for students to practice.
 */


/*
 * Changed this managed bean to be instantiated as eager
 * And Set it to Session Scoped
 */
@ManagedBean(name = "loanManagedBean", eager = true)
@SessionScoped
public class LoanManagedBean implements Serializable {

    @EJB
    MonthlyPaymentCalculator calculator;
	
	private Loan loan;

    public LoanManagedBean() {
        this.loan = new Loan();

    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    
    /*
     * This method calculates the monthly payment via the injected EJB class MonthlyPaymentCalculator
     * It passes the parameters to the MonthlyPaymentCalculator's  calculate method which returns the calculated value 
     * This value is stored inside the Loan object.   
     */
    public String calculate() {
        double monthlyPayment = this.calculator.calculate(loan.getPrinciple(), loan.getNumberOfYears(), loan.getInterestRate());
        this.loan.setMonthlyPayment(monthlyPayment);
        return "index";
    }
}
